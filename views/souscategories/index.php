<?php if ($articles !== []): ?>
    <nav class="d-none d-lg-block" aria-label="breadcrumb">
        <ol class="d-flex pl-0">
            <li class="breadcrumb-item text-muted">
                <p><?= ucfirst($articles[0]['category_title']) ?></p>
            </li>
            <li class="breadcrumb-item">
                <p><?= $articles[0]['subcat_title'] ?></p>
            </li>
        </ol>
    </nav>
    <section id="subcat" class="row">

        <?php $index = [2];
        foreach ($articles as $k => $article):
        $index[] = $index[$k]+3;
        if (in_array($k, $index, true)): $m = ' m-auto';
        else: $m = '';
        endif;
        ?>
            <div class="col-lg-4 mb-3">
                <div class="card text-decoration-none<?= $m ?>">
                    <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                        <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'].'/'.$article['image_name'] ?>"
                             alt="<?= str_replace('.jpg', '', $article['image_name']) ?>"
                             class="w-100">
                    </a>
                    <div class="card-title d-flex justify-content-between px-2">
                        <p class="card-text m-0">
                            <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                                <?= $article['article_title'] ?>
                            </a>
                        </p>
                    </div>
                    <div class="card-body">
                        <p class="text-truncate"><?= $article['article_description'] ?></p>
                        <?php if ($article['article_promo']): ?>
                        <div class="d-flex justify-content-between">
                            <span class="lead">
                            <?php if (strpos($article['article_price'], '.')):?>
                                <s><?= str_replace('.','€', $article['article_price']) ?></s>
                            <?php else : ?>
                                <s><?= $article['article_price'] ?> €</s>
                            <?php endif; ?>
                            </span>
                            <span class="lead text-mca-danger">
                                <?php $promo =  round($article['article_price'] - $article['article_promo']/100*$article['article_price'], 2) ?>
                                <?php if (strpos($promo, '.')):?>
                                    <?= str_replace('.','€', $promo) ?>
                                <?php else : ?>
                                    <?= $promo ?> €
                                <?php endif; ?> <small>(-<?=$article['article_promo']?>%)</small>
                            </span>
                            <?php else : ?>
                            <div class="text-center">
                                <span class="lead">
                               <?= $article['article_price']?>€
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </section>
<?php else: ?>
    <p>Aucun article pour le moment. Revenez plus tard !</p>
<?php endif; ?>
