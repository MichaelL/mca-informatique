<div class="wrapper default-template">
    <!-- wrapper -->

    <?php
    if(false !== strpos($_SERVER['REQUEST_URI'], "admin")):
        include 'views/admin/include/header-admin.php';
        include 'views/admin/include/navbar-admin.php';
        include 'views/admin/include/sidebar-admin.php';
        $padding = ' p-0';
    else:
        include 'views/include/header.php';
        include 'views/include/navbar.php';
        $padding = ' p-5';
        ?>
        <img src="<?=WEBROOT?>assets/images/ReassuranceBanner.jpg" alt=""
        class="banner">
    <?php endif;?>


    <main class="container-fluid<?= $padding ?>" role="main">
        <?= $content ?>
    </main>

<?php
if(false !== strpos($_SERVER['REQUEST_URI'], "admin")):
    include 'views/admin/include/footer-admin.php';
else: include 'views/include/footer.php';
endif;?>