<div class="wrapper account-template">
    <!-- wrapper -->

    <?php include 'views/include/header.php'; ?>

    <?php include 'views/include/navbar.php' ?>

    <main class="container-fluid p-5" role="main">
        <?php if (isset($_SESSION['Alert'])): ?>
            <?= $this->alert() ?>
        <?php endif; ?>
        <?= $content ?>
    </main>

<?php include 'views/include/footer.php'; ?>
