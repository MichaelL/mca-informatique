<div id="create_update">
    <?php if (isset($_SESSION['Alert'])): ?>
        <?= $this->alert() ?>
    <?php endif; ?>
    <h2>Créer une sous-catégorie <?= substr($category['category_title'], 0, -1) ?></h2>

    <form method="post" class="form-row mt-5">
        <label for="title" class="col-12">
            Titre de la sous-catégorie :
            <input id="title" name="title" type="text" class="form-control" required>
        </label>

        <label for="description" class="col-12">
            Description de la sous-catégorie :
            <input id="description" name="description" type="text" class="form-control" required>
        </label>

        <div class="col text-center">
            <button id="create" type="submit" class="btn btn-primary">Créer</button>
        </div>
    </form>
</div>