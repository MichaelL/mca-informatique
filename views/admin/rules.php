<div class="text-right m-3">
    <a href="admin/rule_create"
       class="btn btn-primary">
        Créer un rôle
    </a>
</div>

<div class="table-responsive">
    <table class="table table-hover table-dark m-0">
        <thead>
        <tr>
            <th scope="col" class="lead">Rôles</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
<?php
// Ceci est une boucle foreach pour afficher l'ensemble des catégories
foreach ($rules as $k => $rule):
    ?>
        <tr>
            <td><?= $rule['rule_title'] ?></td>
            <td class="text-right">
                <?php if ($rule['rule_title'] !== 'administrateur' && $rule['rule_title'] !== 'client'): ?>
                <a href="admin/rule_update/<?= $rule['rule_title'] ?>" class="btn btn-dark mr-3">
                    Modifier
                </a>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-rule-<?= $rule['rule_id'] ?>">
                    Supprimer
                </button>

                 <!-- Modal Suppression -->
                <?php
                    $id = "delete-rule-".$rule['rule_id'];
                    $message = "Êtes-vous sûr de supprimer le rôle ".$rule['rule_title']." ?";
                    $buttons = [
                            ['type' => "close"],
                        [
                            'type' => "a",
                            'link' => "admin/rule_delete/".$rule['rule_id'],
                            'name' => "Supprimer",
                            'color' => "danger"
                        ]
                    ];
                echo $this->modal($id, $message, $buttons); ?>

                <?php endif; ?>
            </td>
        </tr>
<?php
endforeach;
?>
        </tbody>
    </table>
</div>
