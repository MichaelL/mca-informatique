<?php
$check = $_POST['dispo'] ?? '';
if ($check) {
    $check = "checked";
}
?>
<div id="create_update">
    <?php if (isset($_SESSION['Alert'])): ?>
        <?= $this->alert() ?>
    <?php endif; ?>
    <h2>Créer un article</h2>

    <form method="post" class="form-row mt-5" enctype="multipart/form-data">
        <label for="title" class="col-12">
            Titre de l'article :
            <input id="title" name="title" type="text" class="form-control"
                   value="<?= $title = $_POST['title'] ?? '' ?>" required>
        </label>

        <label for="article_description" class="col-12">
            Description de l'article :
            <textarea id="article_description" name="description" type="text" class="form-control"
                      maxlength="255" required>
<?= $title = $_POST['description'] ?? '' ?></textarea>
            <span id="nbChar"></span>
        </label>

        <label for="price" class="col-2">
            Prix :
            <input id="price" name="price" type="number" min="1" step="0.01" class="form-control"
                   value="<?= $title = $_POST['price'] ?? '' ?>" required>
        </label>

        <label for="promo" class="col-2">
            Promo :
            <input id="promo" name="promo" type="number" min="0" class="form-control"
                   value="<?= $title = $_POST['promo'] ?? '' ?>">
        </label>

        <label for="date" class="col-6">
            Date :
            <input id="date" name="date" type="date" class="form-control"
                   value="<?= $title = $_POST['date'] ?? '' ?>" required>
        </label>

        <label for="dispo" class="col-2 text-center">
            Disponibilité
            <input id="dispo" name="dispo" type="checkbox" class="form-control"
                   <?= $check ?>>
        </label>

        <label for="image" class="col-12">
            Ajouter une image :
            <input id="image" name="image" type="file" class="form-control" required>
        </label>

        <label for="subcat" class="col-12">
            Sous-Catégorie :
            <select name="subcat" id="subcat" class="form-control" required>
                <option value="">--Veuillez sélectionner une option--</option>
                <?php foreach ($categories as $k => $category):
                    if ($subcat[$k] !== []): ?>
                <optgroup label="<?= $category['category_title'] ?>">
                <?php foreach ($subcat[$k] as $value): ?>
                    <option value="<?= $value['subcat_id'] ?>"
                        <?= $select = (isset($_POST['subcat']) && $value['subcat_id'] === $_POST['subcat']) ? 'selected' : '' ?>>
                        <?= $value['subcat_title'] ?>
                    </option>
                <?php endforeach; ?>
                </optgroup>
                <?php endif;
                endforeach; ?>
            </select>
        </label>

        <div class="col text-center">
            <button id="create" type="submit" class="btn btn-primary">Créer</button>
        </div>
    </form>
</div>