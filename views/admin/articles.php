<div class="text-right m-3">
    <a href="admin/article_create"
       class="btn btn-primary">
        Créer un article
    </a>
</div>

<?php if ($articles !== []): ?>
<div id="view-articles" class="bg-dark-mca text-light">
    <?php foreach ($articles as $k => $article): ?>
    <div class="row mx-0 py-3">
        <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'] . '/' . $article['image_name'] ?>"
             class="col-lg-1 col-sm-3"
             alt="<?= str_replace('.jpg', '', $article['image_name']) ?>">
        <div class="col-lg-5 col-sm-5">
            <h5 class="article-title mt-0"><?= $article['article_title'] ?>
                <?= $dispo = ($article['article_dispo'])? '<i class="bi bi-circle-fill text-success" title="disponible"></i>': '<i class="bi bi-circle-fill text-mca-danger" title="indisponible"></i>' ?>
            </h5>
            <span class="article-badge badge badge-light"><?= $article['subcat_title'] ?></span>
        </div>
        <div class="col-lg-6 col-sm-4 text-right my-auto">
            <a href="admin/article_update/<?= $article['article_url'] ?>" class="btn btn-dark mr-3">
                Éditer
            </a>
            <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#delete-article-<?= $article['article_id'] ?>'>
                Supprimer
            </button>

            <!-- Modal Suppression -->
            <?php
            $id = "delete-article-".$article['article_id'];
            $message = "Êtes-vous sûr de supprimer ".$article['article_title']." ?";
            $buttons = [
                ['type' => "close"],
                [
                    'type' => "a",
                    'link' => "admin/article_delete/".$article['article_id'],
                    'name' => "Supprimer",
                    'color' => "danger"
                ]
            ];
            echo $this->modal($id, $message, $buttons); ?>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>
