<div id="create_update">
    <?php if (isset($_SESSION['Alert'])): ?>
        <?= $this->alert() ?>
    <?php endif; ?>
    <h2>Modifier un rôle</h2>

    <form method="post" class="form-row mt-5">
        <label for="title" class="col-12">
            Titre du rôle :
            <input id="title" name="title" type="text" class="form-control"
                   value="<?= $title = $_POST['title'] ?? $rule['rule_title'] ?>" required>
        </label>

        <label for="description" class="col-12">
            Description du rôle :
            <input id="description" name="description" type="text" class="form-control"
                   value="<?= $description = $_POST['description'] ?? $rule['rule_description'] ?>" required>
        </label>

        <div class="col text-center">
            <button id="update" type="submit" class="btn btn-mca">Enregistrer</button>
        </div>
    </form>
</div>