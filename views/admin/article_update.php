<?php
$check = $_POST['dispo'] ?? $article['article_dispo'];
if ($check) {
    $check = "checked";
}
?>
<div id="create_update">
    <?php if (isset($_SESSION['Alert'])): ?>
        <?= $this->alert() ?>
    <?php endif; ?>
    <h2>Modifier un article</h2>

    <form method="post" class="form-row mt-5" enctype="multipart/form-data">
        <label for="title" class="col-12">
            Titre de l'article :
            <input id="title" name="title" type="text" class="form-control"
                   value="<?= $title = $_POST['title'] ?? $article['article_title'] ?>" required>
        </label>

        <label for="article_description" class="col-12">
            Description de l'article :
            <textarea id="article_description" name="description" type="text" class="form-control"
                      maxlength="255" required>
<?= $description = $_POST['description'] ?? $article['article_description'] ?></textarea>
            <span id="nbChar"></span>
        </label>

        <label for="price" class="col-2">
            Prix :
            <input id="price" name="price" type="number" min="1" step="0.01" class="form-control"
                   value="<?= $price = $_POST['price'] ?? $article['article_price'] ?>" required>
        </label>

        <label for="promo" class="col-2">
            Promo :
            <input id="promo" name="promo" type="number" min="0" class="form-control"
                   value="<?= $promo = $_POST['promo'] ?? $article['article_promo'] ?>">
        </label>

        <label for="date" class="col-6">
            Date :
            <input id="date" name="date" type="date" class="form-control"
                   value="<?= $date = $_POST['date'] ?? $article['article_date'] ?>" required>
        </label>

        <label for="dispo" class="col-2 text-center">
            Disponibilité
            <input id="dispo" name="dispo" type="checkbox" class="form-control"
                   <?= $check ?>>
        </label>

        <label for="image" class="col-12">
            Ajouter une image :
            <input id="image" name="image" type="file" class="form-control">
        </label>

        <div class="col-12 row">
        <?php foreach ($article['images'] as $k => $image): ?>
        <div class="col-auto">
            <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'] ?>/<?= $article['images'][$k]['image_name'] ?>" alt="<?= str_replace('.jpg', '', $article['images'][$k]['image_name']) ?>"
            class="uploaded-image">
            <button type='button' class='btn btn-danger delete-image' data-toggle='modal' data-target='#delete-image-<?= $article['images'][$k]['image_id'] ?>'>
                <i class="bi bi-folder-x"></i>
            </button>
        </div>
            <!-- Modal Suppression -->
            <?php
            $id = "delete-image-".$article['images'][$k]['image_id'];
            $message = "Êtes-vous sûr de supprimer ".$article['images'][$k]['image_name']." ?";
            $buttons = [
                ['type' => "close"],
                [
                    'type' => "a",
                    'link' => "../image_delete/".$article['images'][$k]['image_id'],
                    'name' => "Supprimer",
                    'color' => "danger"
                ]
            ];
            echo $this->modal($id, $message, $buttons); ?>
        <?php endforeach; ?>
        </div>

        <label for="subcat" class="col-12">
            Sous-Catégorie :
            <select name="subcat" id="subcat" class="form-control" required>
                <?php foreach ($categories as $k => $category):
                    if ($subcat[$k] !== []): ?>
                        <optgroup label="<?= $category['category_title'] ?>">
                            <?php foreach ($subcat[$k] as $value): ?>
                                <option value="<?= $value['subcat_id'] ?>"
                                    <?= $select = ($value['subcat_id'] === $article['article_subcat']) ? 'selected' : '' ?>>
                                    <?= $value['subcat_title'] ?>
                                </option>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endif;
                endforeach; ?>
            </select>
        </label>

        <div class="col text-center">
            <button id="update" type="submit" class="btn btn-mca">Enregistrer</button>
        </div>
    </form>
</div>