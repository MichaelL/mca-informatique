<div class="accordion" id="view-users">
    <?php
    // Ceci est une boucle foreach pour afficher l'ensemble des catégories
    foreach ($rules as $k => $rule):
        ?>
        <div class="card bg-dark-mca text-light rounded-0">
            <div class="card-header" id="<?= $rule['rule_title'] ?>"
                 type="button" data-toggle="collapse"
                 data-target="#collapse-<?= $rule['rule_id'] ?>"
                 aria-expanded="false"
                 aria-controls="collapse-<?= $rule['rule_id'] ?>">
                <div class="d-flex justify-content-between lead my-2">
                    Rôle <?= $rule['rule_title'] ?>
                </div>
            </div>

            <div id="collapse-<?= $rule['rule_id'] ?>" class="collapse <?= $show = ($k === 0)? 'show':'' ?>"
                 aria-labelledby="<?= $rule['rule_id'] ?>" data-parent="#view-users">

                <?php if ($users[$k] !== []): ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-dark m-0">
                            <thead>
                            <tr>
                                <th scope="col">Nom Prénom</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users[$k] as $user): ?>
                                <tr>
                                    <td><?= $user['user_lastname'].' '.$user['user_firstname'] ?></td>
                                    <td class="text-right">
                                        <form method="post" class="m-auto">
                                            <div class="d-inline-block">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text bg-dark-mca text-light border-0" for="rule">Rôle</label>
                                                    </div>
                                                    <select name="rule" id="rule" class="form-control bg-dark-mca text-light border-dark">
                                                        <?php
                                                        foreach ($rules as $role): ?>
                                                            <option value="<?= $role['rule_id'] ?>"
                                                                <?= $select = ($user['user_rule'] === $role['rule_id']) ? 'selected' : '' ?>>
                                                                <?= $role['rule_title'] ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <input name="id" id="id" type="hidden" value="<?= $user['user_id'] ?>">
                                                    <div class="input-group-append">
                                                        <button id="user_update" type="submit" class="btn btn-dark mr-3">
                                                            Enregistrer
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-user-<?= $user['user_id'] ?>">
                                                Supprimer
                                            </button>
                                        </form>

                                        <!-- Modal Suppression -->
                                       <?php
                                       $id = "delete-user-".$user['user_id'];
                                       $message = "Êtes-vous sûr de supprimer ".$user['user_lastname']." ".$user['user_firstname']." ?";
                                       $buttons = [
                                           ['type' => "close"],
                                           [
                                               'type' => "a",
                                               'link' => "admin/user_delete/".$user['user_id'],
                                               'name' => "Supprimer",
                                               'color' => "danger"
                                           ]
                                       ];
                                       echo $this->modal($id, $message, $buttons); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <div class="card-body">
                        <p>Aucun rôle</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php
    endforeach;
    ?>
</div>
