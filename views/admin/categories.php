<div class="accordion" id="view-subcat">
    <?php
    // Ceci est une boucle foreach pour afficher l'ensemble des catégories
    foreach ($categories as $k => $category):
        ?>
        <div class="card bg-dark-mca text-light rounded-0">
            <div class="card-header" id="<?= $category['category_url'] ?>"
                 type="button" data-toggle="collapse"
                 data-target="#collapse-<?= $category['category_url'] ?>"
                 aria-expanded="false"
                 aria-controls="collapse-<?= $category['category_url'] ?>">
                <div class="d-flex justify-content-between lead my-2">
                    Catégorie <?= $category['category_title'] ?>

                    <a href="admin/subcat_create/<?= $category['category_url'] ?>"
                       class="btn btn-primary">
                        Créer une sous-catégorie
                    </a>
                </div>
            </div>

            <div id="collapse-<?= $category['category_url'] ?>" class="collapse <?= $show = ($k === 0)? 'show':'' ?>"
                 aria-labelledby="<?= $category['category_url'] ?>" data-parent="#view-subcat">

                <?php if ($subcat[$k] !== []): ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-dark m-0">

                            <tbody>
                            <?php foreach ($subcat[$k] as $value): ?>
                                <tr>
                                    <td><?= $value['subcat_title'] ?></td>
                                    <td class="text-right">
                                        <a href="admin/subcat_update/<?= $value['subcat_url'] ?>" class="btn btn-dark mr-3">
                                            Éditer
                                        </a>
                                        <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#delete-subcat-<?= $value['subcat_id'] ?>'>
                                            Supprimer
                                        </button>

                                        <!-- Modal Suppression -->
                                        <?php
                                        $id = "delete-subcat-".$value['subcat_id'];
                                        $message = "Êtes-vous sûr de supprimer ".$value['subcat_title']." ?";
                                        $buttons = [
                                            ['type' => "close"],
                                            [
                                                'link' => "admin/subcat_delete/".$value['subcat_id'],
                                                'name' => "Supprimer",
                                                'color' => "danger"
                                            ]
                                        ];
                                        echo $this->modal($id, $message, $buttons); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <div class="card-body">
                        <p>Aucune sous-catégorie</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php
    endforeach;
    ?>
</div>
