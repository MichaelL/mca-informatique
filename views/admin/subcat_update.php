<div id="create_update">
    <?php if (isset($_SESSION['Alert'])): ?>
        <?= $this->alert() ?>
    <?php endif; ?>
    <h2>Modifier une sous-catégorie</h2>

    <form method="post" class="form-row mt-5">
        <label for="title" class="col-12">
            Titre de la sous-catégorie :
            <input id="title" name="title" type="text" class="form-control"
                   value="<?= $title = $_POST['title'] ?? $subcat['subcat_title'] ?>" required>
        </label>

        <label for="description" class="col-12">
            Description de la sous-catégorie :
            <input id="description" name="description" type="text" class="form-control"
                   value="<?= $description = $_POST['description'] ?? $subcat['subcat_description'] ?>" required>
        </label>

        <label for="category" class="col-12">
            Catégorie :
            <select name="category" id="category" class="form-control">
                <?php foreach ($categories as $category): ?>
                    <option value="<?= $category['category_id'] ?>"
                        <?= $select = ($subcat['subcat_category'] === $category['category_id']) ? 'selected' : '' ?>>
                        <?= $category['category_title'] ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </label>

        <div class="col text-center">
            <button id="update" type="submit" class="btn btn-mca">Enregistrer</button>
        </div>
    </form>
</div>