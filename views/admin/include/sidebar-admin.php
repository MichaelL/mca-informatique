<div id="navbar-docs" class="collapse">
    <div class="list-group rounded-0" id="list-tab" role="tablist">
        <?php
        $params = explode('/', $_GET['p']);
        $controller = ucfirst($params[0]).'Controller';
        $function = $params[1]??'index';
        if ($_SERVER['REQUEST_URI'] !== '/mca-informatique/admin' && method_exists($controller, $function)): ?>
            <a class="list-group-item active"
               href="<?= WEBROOT ?>admin">Retour</a>
        <?php else: ?>
            <a class="list-group-item list-group-item-action active" id="list-accueil"
               data-toggle="list" href="#accueil" role="tab"
               aria-controls="accueil">Tableau de bord</a>
            <a class="list-group-item list-group-item-action" id="list-roles"
               data-toggle="list" href="#roles" role="tab"
               aria-controls="roles">Roles</a>
            <a class="list-group-item list-group-item-action" id="list-users"
               data-toggle="list" href="#users" role="tab"
               aria-controls="roles">Utilisateurs</a>
            <a class="list-group-item list-group-item-action" id="list-sous-categories"
               data-toggle="list" href="#sous-categories" role="tab"
               aria-controls="sous-categories">Sous-catégories</a>
            <a class="list-group-item list-group-item-action" id="list-articles"
               data-toggle="list" href="#articles" role="tab"
               aria-controls="articles">Articles</a>
            <a class="nav-link btn-danger admin-logout" href="<?= WEBROOT ?>logout.php">Se déconnecter</a>
        <?php endif; ?>
    </div>
</div>
