<nav class="navbar navbar-expand-lg navbar-dark bg-dark-mca position-sticky navbar-admin">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-docs"
            aria-controls="navbar-docs" aria-expanded="false" aria-label="Toggle docs navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="<?= WEBROOT ?>">
        <img src="<?= WEBROOT ?>assets/images/LogoMCA.png" alt="logo MCA Informatique"
             id="logo-navbar">
    </a>
</nav>