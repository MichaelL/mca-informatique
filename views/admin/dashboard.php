<div class="row m-0">
    <div class="col-lg-12" id="admin_dashboard">
        <h2>Tableau de bord administrateur</h2>
        <div class="case-content text-center">
            <?php if (isset($_SESSION['Alert'])): ?>
                <?= $this->alert() ?>
            <?php else: ?>
                <div class="alert alert-success">Site opérationnel : Aucune erreur détecté</div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-12" id="admin_users">
        <h2>Role utilisateurs</h2>
        <div class="case-content">
            <?php foreach ($rules as $k => $rule):
            if (count($users[$k]) === 0): ?>
            <p><b>Aucun <?= ucfirst($rule['rule_title']) ?></b>
                <?php elseif (count($users[$k]) === 1): ?>
            <p><b>1 <?= ucfirst($rule['rule_title']) ?></b>
                <?php else: ?>
            <p><b><?= count($users[$k]).' '.ucfirst($rule['rule_title']).'s' ?></b>
                <?php endif;
                endforeach;?>
        </div>
    </div>

    <div class="col-lg-6 pl-0">
        <div id="admin_articles">
            <h2>Articles</h2>
            <div class="case-content">
                <?php if (count($articles) === 0): ?>
                    <p><b>Aucun article</b> enregistré</p>
                <?php elseif (count($articles) === 1): ?>
                    <p><b>1 article</b> enregistré</p>
                <?php else: ?>
                    <p><b><?= count($articles) ?> articles</b> enregistrés</p>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-lg-6 p-0">
        <div id="admin_categories">
            <h2>Sous-catégories</h2>
            <div class="case-content">
                <?php foreach ($categories as $k => $category):
                if (count($subcat[$k]) === 0): ?>
                <p><b>Aucune sous-catégorie</b> enregistrée
                    <?php elseif (count($subcat[$k]) === 1): ?>
                <p><b>1 sous-catégorie</b> enregistrée
                    <?php else: ?>
                <p><b><?= count($subcat[$k]) ?> sous-catégories</b> enregistrées
                    <?php endif;
                    echo 'dans <b>'.ucfirst($category['category_title']).'</b></p>';
                    endforeach;?>
            </div>
        </div>
    </div>
</div>