<div class="tab-content" id="nav-tabContent">

    <div class="tab-pane p-5 show active" id="accueil" role="tabpanel" aria-labelledby="list-accueil">
        <?php include 'dashboard.php'?>
    </div>

    <div class="tab-pane pt-4 mb-5" id="roles" role="tabpanel" aria-labelledby="list-roles">
        <?php include 'rules.php'?>
    </div>

    <div class="tab-pane pt-5 mb-5" id="users" role="tabpanel" aria-labelledby="list-users">
        <?php include 'users.php'?>
    </div>

    <div class="tab-pane pt-5 mb-5" id="sous-categories" role="tabpanel" aria-labelledby="list-sous-categories">
        <?php include 'categories.php'?>
    </div>

    <div class="tab-pane pt-4 mb-5" id="articles" role="tabpanel" aria-labelledby="list-articles">
        <?php include 'articles.php'?>
    </div>

</div>