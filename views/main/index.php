<?php if (isset($articles)): ?>
    <section class="row mb-4">
        <div class="col-12">
            <h2 class="mb-0 pb-4 text-center text-light bg-danger-mca"> Les offres à ne pas manquer </h2>
        </div>

        <?php if ($articles['promo']):
        foreach ($articles['promo'] as $article):?>
            <div class="col-lg-3 mb-3">
                <div class="card" style="overflow: hidden">
                    <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                        <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'].'/'.$article['image_name'] ?>"
                             alt="<?= str_replace('.jpg', '', $article['image_name']) ?>"
                             class="w-100">
                    </a>
                    <div class="card-title d-flex justify-content-between px-2">
                        <p class="card-text m-0">
                            <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                                <?= $article['article_title'] ?>
                            </a>
                        </p>
                    </div>
                    <div class="card-body">
                        <p class="text-truncate"><?= $article['article_description'] ?></p>
                        <div class="d-flex justify-content-between">
                            <span class="lead">
                                <?php
                                if (strpos($article['article_price'], '.')):
                                ?>
                            <s><?= str_replace('.','€', $article['article_price']) ?></s>
                                <?php else : ?>
                                    <s><?= $article['article_price'] ?> €</s>
                                <?php endif; ?>
                            </span>
                            <span class="lead text-mca-danger">
                                <?php $promo =  round($article['article_price'] - $article['article_promo']/100*$article['article_price'], 2) ?>
                            <?php
                            if (strpos($promo, '.')):
                                ?>
                                <?= str_replace('.','€', $promo) ?>
                            <?php else : ?>
                                <?= $promo ?> €
                            <?php endif; ?> <small>(-<?=$article['article_promo']?>%)</small>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;
        else: echo "<p class='col-12 text-center'>Restez à l'affût. De nouvelles promos arrivent.</p>";
        endif; ?>
    </section>

    <section class="row mt-4">
        <div class="col-12">
            <h2 class="mb-0 pb-4 text-center text-light bg-danger-mca"> Nouveautés </h2>
        </div>
        <?php foreach ($articles['new'] as $article): ?>
            <div class="col-lg-3 mb-3">
                <div class="card text-decoration-none">
                    <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                    <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'].'/'.$article['image_name'] ?>"
                         alt="<?= str_replace('.jpg', '', $article['image_name']) ?>"
                    class="w-100">
                    </a>
                    <div class="card-title d-flex justify-content-between px-2">
                        <p class="card-text m-0">
                            <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                            <?= $article['article_title'] ?>
                            </a>
                        </p>
                    </div>
                    <div class="card-body">
                        <p class="text-truncate"><?= $article['article_description'] ?></p>
                        <div class="text-center">
                            <span class="lead">
                               <?= $article['article_price']?>€
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </section>
<?php else: echo "<p class='text-center'>Aucun article. Revenez plus tard.</p>";
endif; ?>