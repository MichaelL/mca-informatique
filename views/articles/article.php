<nav class="d-none d-lg-block" aria-label="breadcrumb">
    <ol class="d-flex">
        <li class="breadcrumb-item text-muted">
            <p><?= ucfirst($article['category_title']) ?></p>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= WEBROOT.'souscategories/'.$article['subcat_url'] ?>"><?= $article['subcat_title'] ?></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <?= $article['article_title'] ?>
        </li>
    </ol>
</nav>

<div class="row mx-0">
    <div class="col-lg-8 article-images row">
        <?php if (count($article['images']) > 1): ?>
            <div class="col-2 image-secondary">
            <?php foreach ($article['images'] as $k => $image):
                if ($k !== 0): ?>
                <img src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'] ?>/<?= $image['image_name'] ?>" alt="<?= str_replace('.jpg', '', $image['image_name']) ?>">
            <?php endif;
            endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="col image-primary" style='
        background: #fff url("<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'] ?>/<?= $article['images'][0]['image_name'] ?>") no-repeat center;
        background-size: contain;'>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="bg-white py-5">
            <span class="dispo">
                <?= $dispo = ($article['article_dispo'])? '<i class="bi bi-circle-fill text-success" title="disponible"></i> En stock': '<i class="bi bi-circle-fill text-mca-danger" title="indisponible"></i> Stock épuisé' ?>
            </span>
            <span class="badge-dark subcat py-1 pl-3 pr-5"><?= $article['subcat_title'] ?></span>

            <?php if ($article['article_promo']): ?>
            <div class="d-flex justify-content-around my-5">
                <span class="lead">
                <?php if (strpos($article['article_price'], '.')):?>
                    <s><?= str_replace('.','€', $article['article_price']) ?></s>
                <?php else : ?>
                    <s><?= $article['article_price'] ?> €</s>
                <?php endif; ?>
                </span>
                <span class="lead text-mca-primary article-price">
                    <?php $promo =  round($article['article_price'] - $article['article_promo']/100*$article['article_price'], 2) ?>
                    <?php if (strpos($promo, '.')):?>
                        <?= str_replace('.','€', $promo) ?>
                    <?php else : ?>
                        <?= $promo ?> €
                    <?php endif; ?> <small>(-<?=$article['article_promo']?>%)</small>
                </span>
                <?php else : ?>
                <div class="text-center my-5">
                    <span class="lead text-mca-primary article-price">
                   <?= $article['article_price']?>€
                    </span>
                <?php endif; ?>
            </div>
            <div class="text-center">
                <b class="m-0"><?= $article['article_title'] ?></b>
            </div>
            <div class="text-center my-5">
                <?= $addToCart = ($article['article_dispo'])?
                    '<button id="addToCart" data-article="'.$article['article_id'].'" class="btn btn-primary rounded-0 w-100"><i class="bi bi-cart-plus-fill"></i> AJOUTER AU PANIER</button>':
                    '<a href="#">Être alerté lors du réapprovisionnement</a>' ?>
            </div>
            <p class="px-4"><?= $article['article_description'] ?></p>
        </div>
    </div>
</div>
