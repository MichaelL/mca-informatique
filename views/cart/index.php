<h2 id="cartTitle"><?= $ifEmpty = (empty($articles))? 'Panier vide' : '' ?></h2>

<?php
foreach ($articles as $article): ?>
    <div class="bg-white p-4 mb-4">
        <div class="py-1 d-flex">
            <img class="col-lg-1 col-sm-2 p-0 mr-3"
                 src="<?= WEBROOT ?>assets/images/articles/<?= $article['article_url'] ?>/<?= $article['image_name'] ?>"
                 alt="<?= str_replace('.jpg', '', $article['image_name']) ?>">
            <div class="col">
                <h5 class="position-absolute fixed-top ml-3 mb-0">
                    <a href="<?= WEBROOT ?>articles/<?= $article['article_url'] ?>">
                        <?= $article['article_title'] ?>
                    </a>
                </h5>
                <p class="lead position-absolute fixed-bottom ml-3 mb-0 price">
                    <?php if ($article['article_promo']):
                        echo round($article['article_price'] - $article['article_promo']/100*$article['article_price'], 2) ?>
                    <?php else:
                        echo $article['article_price'];
                    endif; ?>
                    €
                </p>
            </div>
            <div class="d-block text-right my-auto">
                <button data-article="<?= $article['article_id'] ?>" class="btn btn-danger removeToCart">
                    <i class="bi bi-cart-dash-fill"></i>
                </button>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php if ($articles): ?>
<div id="total_price" class="lead text-right"></div>

<div class="text-center">
    <button id="commande" class="btn btn-mca">
        Commander
    </button>
</div>
<?php endif; ?>