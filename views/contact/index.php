<div class="container container-login">

    <form method="post" class="form-row">
        <label for="mail" class="col-12">
            <input id="mail" name="mail" type="email" class="form-control"
                   value="<?= $mail = $_SESSION['auth']['user_mail'] ?? '' ?>"
                   placeholder="Adresse e-mail" required>
        </label>

        <label for="subject" class="col-12">
            <input id="subject" name="subject" type="text" class="form-control"
                   placeholder="Sujet" required>
        </label>
        <label for="message" class="col-12">
            <textarea name="message" class="form-control" required></textarea>
        </label>
        <div class="col-12 text-center">
            <button id="contact" type="submit" class="btn btn-mca">Envoyer</button>
        </div>
    </form>

</div>