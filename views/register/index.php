<div class="container container-login">

    <form method="post" class="form-row">
        <label for="firstname" class="col-lg-6">
            <input id="firstname" name="firstname"
                   type="text" class="form-control"
                   placeholder="Prénom" required>
        </label>
        <label for="lastname" class="col-lg-6">
            <input id="lastname" name="lastname"
                   type="text" class="form-control"
                   placeholder="Nom" required>
        </label>
        <label for="mail" class="col-12">
            <input id="mail" name="mail" type="email" class="form-control"
                   placeholder="Adresse e-mail" required>
        </label>
        <label for="password" class="col-12">
            <input id="password" name="password" type="password" class="form-control"
                   placeholder="Mot de passe" required>
        </label>
        <div class="col-12 my-4 text-center">
            <a href="<?= WEBROOT ?>login" class="text-light lead">Déjà client ?</a>
        </div>
        <div class="col-12 text-center">
            <button id="register" type="submit" class="btn btn-mca">Valider</button>
        </div>
    </form>

</div>