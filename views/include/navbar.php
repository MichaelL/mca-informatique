<nav class="navbar navbar-expand-lg navbar-dark bg-dark-mca sticky-top">
    <a class="navbar-brand" href="<?= WEBROOT ?>">
        <img src="<?= WEBROOT ?>assets/images/LogoMCA.png" alt="logo MCA Informatique"
        id="logo-navbar">
    </a>
    <div class="collapse navbar-collapse" id="navbar">
        <div class="navbar-nav">
            <?php
            $db = new PDO('mysql:host=localhost;dbname=mca', 'root', '');
            $categories = $db->query("SELECT category_id,category_title,category_color FROM categories")->fetchAll(PDO::FETCH_ASSOC);

            foreach ($categories as $category) {
                $subcat[] = $db->query("SELECT subcat_id,subcat_title,subcat_url FROM subcat WHERE subcat_category=" . $category['category_id'])->fetchAll(PDO::FETCH_ASSOC);
            }

            foreach ($categories as $k => $category):
                if ($subcat[$k] !== []):?>
            <div class="dropdown nav-link"
                 data-color="<?= $category['category_color'] ?>">
                <button type="button" title="<?= $category['category_title'] ?>" id="<?= $category['category_title'] ?>"
                        class="btn shadow-none text-light"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= strtoupper($this->upperSpecialChar($category['category_title'])) ?>
                </button>
                <div class="dropdown-menu m-0" aria-labelledby="<?= $category['category_title'] ?>">
                <?php foreach ($subcat[$k] as $value): ?>
                    <a class="nav-link" href="<?= WEBROOT.'souscategories/'.$value['subcat_url'] ?>"><?= $value['subcat_title'] ?></a>
                    <div class="dropdown-divider m-0"></div>
                <?php endforeach;?>
                </div>
            </div>
            <?php endif;
            endforeach; ?>
        </div>
    </div>
    <form action="<?= WEBROOT ?>cart" method="post" class="m-0">
        <button class="btn mr-3 cart">
            <span class="badge-danger"></span>
            <i class="bi bi-cart-fill text-light"></i>
        </button>
        <input type="hidden" name="cart">
    </form>
    <?php if (!isset($_SESSION['auth'])): ?>

            <a class="nav-link text-light text-center" href="<?= WEBROOT ?>login">
                <i class="bi bi-person-circle"></i>
                <small class="d-block">Identifiez-vous</small></a>
    <?php else: ?>

    <div class="dropdown">
        <button type="button" title="profil" id="profil"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                class="btn text-light
            <?php if(!empty($_SESSION['auth']['user_picture'])): ?>
                p-0">
                <img src="<?= WEBROOT. 'assets/images/users/' .$_SESSION['auth']['user_id'].'/'.$_SESSION['auth']['user_picture'] ?>"
                     alt="mon image de profil">

            <?php else: ?>
                ">
                <i class="bi bi-person-circle"></i>
            <?php endif; ?>
            <small class="d-block">Bonjour <?= $_SESSION['auth']['user_firstname'] ?></small>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profil">
            <?php if ($_SESSION['auth']['user_rule'] === "2"): ?>
                <a class="nav-link btn-outline-primary" href="<?= WEBROOT ?>admin">Admin</a>
            <?php endif; ?>
            <a class="nav-link btn-outline-primary
            <?= $active = (strpos($_SERVER['REQUEST_URI'], "account"))? 'active': '' ?>"
               href="<?= WEBROOT ?>account">Mon compte</a>
            <a class="nav-link btn-danger" href="<?= WEBROOT ?>logout.php">Se déconnecter</a>
        </div>
    </div>
    <?php endif; ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>