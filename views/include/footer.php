<div class="clearfix footer-fix"></div>

<footer class="container-fluid bg-dark-mca py-3 text-light">
    <ul class="m-0">
        <li class="d-block">
            <a class="text-light" href=" <?=WEBROOT ?>">Mentions légales</a>
        </li>
        <li class="d-block">
           <a class="text-light" href="<?=WEBROOT ?>">Conditions générales de vente </a>
        </li>
    </ul>

    <div class="text-center" id="copyright">

    </div>
</footer>

<!-- /wrapper -->
</div>

<script type="application/javascript" src="<?= WEBROOT ?>assets/js/script.js"></script>
<?= $accountJS = (strpos($_SERVER['REQUEST_URI'], "account"))? '<script type="application/javascript" src="'. WEBROOT .'assets/js/account.js"></script>' : '' ?>
</body>
</html>