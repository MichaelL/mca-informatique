<h2 class="mb-4">Informations de contact</h2>

<form method="post" class="form-row" enctype="multipart/form-data">
    <label for="lastname" class="col-lg-4">
        Nom
        <input id="lastname" name="lastname" type="text" class="form-control"
               value="<?= $lastname = $_SESSION['auth']['user_lastname'] ?? '' ?>"
               required>
    </label>

    <label for="firstname" class="col-lg-4">
        Prénom
        <input id="firstname" name="firstname" type="text" class="form-control"
               value="<?= $firstname = $_SESSION['auth']['user_firstname'] ?? '' ?>"
               required>
    </label>

    <label for="phone" class="col-lg-4">
        Téléphone
        <input id="phone" name="phone" type="tel" class="form-control"
            <?php if ($_SESSION['auth']['user_phone']): ?>
                value="<?= $_SESSION['auth']['user_phone'] ?>"
            <?php endif; ?> required>
    </label>

    <p class="col-12 mt-3 mb-0">Modifier vos identifiants de connexion</p>
    <label for="old-password" class="d-none">
        <input id="old-password" name="old-password" type="password">
    </label>

    <label for="mail" class="col-lg-6">
        <input id="mail" name="mail" type="email" class="form-control"
               placeholder="mail : exemple@mail.com" value="<?= $mail = $_SESSION['auth']['user_mail'] ?? '' ?>"
               required>
    </label>

    <label for="password" class="col-lg-6">
        <input id="password" name="password" type="password" class="form-control"
               placeholder="Mot de passe"
               data-toggle="tooltip"
               data-placement="left"
               title="Laissez vide si vous ne souhaitez pas en changer">
    </label>

    <label for="image" class="col-12">
        Ajouter une image personnalisée
        <input id="image" name="image" type="file" class="form-control">
    </label>

    <div class="col text-center">
            <span class="btn btn-mca">
                <input id="user" name="user" type="submit" class="btn p-0" value="Enregistrer" disabled>
            </span>
    </div>
</form>