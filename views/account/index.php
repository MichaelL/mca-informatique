<h2 class="mb-5">Mon compte</h2>

<div id="account" class="row">
    <div class="col-lg-3">

        <div class="accordion" id="view-sidebar">
            <a class="btn btn-link btn-block d-lg-none" type="button" data-toggle="collapse" data-target="#sidebar" aria-expanded="true" aria-controls="sidebar">
                Menu <i class="bi bi-arrow-down-short"></i>
            </a>
            <?php include 'include/sidebar.php' ?>
        </div>


    </div>

    <div class="col-lg-9 tab-content">

        <div class="tab-pane fade show active" id="compte" role="tabpanel" aria-labelledby="list-compte">
            <?php include 'compte.php' ?>
        </div>

        <div class="tab-pane fade" id="infos" role="tabpanel" aria-labelledby="list-infos">
            <?php include 'infos.php' ?>
        </div>

        <div class="tab-pane fade" id="adresses" role="tabpanel" aria-labelledby="list-adresses">
            <?php include 'adresses.php' ?>
        </div>

        <div class="tab-pane fade" id="commands" role="tabpanel" aria-labelledby="list-commands">
            <?php include 'commands.php' ?>
        </div>

    </div>
</div>
