<div class="col-12 text-right">
<label for="address">
    <select name="address" id="address"
    class="form-control">
        <option value="al" selected>Livraison</option>
        <option value="af">Facturation</option>
    </select>
</label>
</div>

<form method="post" class="form-row" id="al">
    <h2 class="mb-4">Adresse de livraison</h2>

    <label for="country" class="col-12">
        Pays
        <input id="country" name="country" type="text" class="form-control"
               placeholder="e.g. : FR" value="<?= $country = $delivery['delivery_country'] ?? '' ?>"
               required>
    </label>

    <label for="number" class="col-2">
        Numéro de rue
        <input id="number" name="number" type="text" class="form-control"
               placeholder="1001" value="<?= $number = $delivery['delivery_number'] ?? '' ?>"
               required>
    </label>

    <label for="street" class="col-10 mt-auto">
        Nom de rue/avenue/boulevard
        <input id="street" name="street" type="text" class="form-control"
               placeholder="Rue Georges Clemenceau" value="<?= $street = $delivery['delivery_street'] ?? '' ?>"
               required>
    </label>

    <label for="zip" class="col-lg-2">
        Code postal
        <input id="zip" name="zip" type="text" class="form-control"
               placeholder="59120" value="<?= $zip = $delivery['delivery_zip'] ?? '' ?>"
               required>
    </label>

    <label for="city" class="col-10">
        Ville
        <input id="city" name="city" type="text" class="form-control"
               placeholder="Loos" value="<?= $city = $delivery['delivery_city'] ?? '' ?>"
               required>
    </label>

    <label for="cedex" class="col-lg-12">
        CEDEX <i>(facultatif)</i>
        <input id="cedex" name="cedex" type="text" class="form-control"
               value="<?= $cedex = $delivery['delivery_cedex'] ?? '' ?>">
    </label>

    <label class="col-lg-12" for="billing-and-delivery">
    <input id="billing-and-delivery" name="billing-and-delivery"
           type="checkbox"/>
        Choisir cette adresse comme adresse de facturation
    </label>


    <div class="col text-center">
        <span class="btn btn-mca">
            <input id="delivery" name="delivery" type="submit" class="btn p-0" value="Enregistrer">
        </span>
    </div>
</form>

<form method="post" class="form-row d-none" id="af">
    <h2 class="mb-4">Adresse de facturation</h2>

    <label for="country" class="col-12">
        Pays
        <input id="country" name="country" type="text" class="form-control"
               placeholder="e.g. : FR" value="<?= $country = $billing['billing_country'] ?? '' ?>"
               required>
    </label>

    <label for="number" class="col-2">
        Numéro de rue
        <input id="number" name="number" type="text" class="form-control"
               placeholder="1001" value="<?= $number = $billing['billing_number'] ?? '' ?>"
               required>
    </label>

    <label for="street" class="col-10 mt-auto">
        Nom de rue/avenue/boulevard
        <input id="street" name="street" type="text" class="form-control"
               placeholder="Rue Georges Clemenceau" value="<?= $street = $billing['billing_street'] ?? '' ?>"
               required>
    </label>

    <label for="zip" class="col-lg-2">
        Code postal
        <input id="zip" name="zip" type="text" class="form-control"
               placeholder="59120" value="<?= $zip = $billing['billing_zip'] ?? '' ?>"
               required>
    </label>

    <label for="city" class="col-10">
        Ville
        <input id="city" name="city" type="text" class="form-control"
               placeholder="Loos" value="<?= $city = $billing['billing_city'] ?? '' ?>"
               required>
    </label>

    <label for="cedex" class="col-lg-12">
        CEDEX <i>(facultatif)</i>
        <input id="cedex" name="cedex" type="text" class="form-control"
               value="<?= $cedex = $billing['billing_cedex'] ?? '' ?>">
    </label>


    <div class="col text-center">
        <span class="btn btn-mca">
            <input id="billing" name="billing" type="submit" class="btn p-0" value="Enregistrer">
        </span>
    </div>
</form>

