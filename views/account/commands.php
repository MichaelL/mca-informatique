<div class="row">
    <div class="col-lg-5 pl-0">
        <div class="bg-white">
            <ul class="list-group list-group-flush p-3">
                <li class="list-group-item p-1">
                    Numéro de commande : <b class="text-dark">0627414FR59</b>
                </li>
                <li class="list-group-item p-1">
                    Date de commande : <i class="text-dark">14/06/2021</i>
                </li>
                <li class="list-group-item p-1">
                    Statut de la Commande : <b class="text-success">Livré le 18/06/2021</b>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-lg-7 d-flex justify-content-between bg-white">
        <div class="m-auto text-center">
            <button class="btn btn-primary rounded-0 p-3 mr-4 view-command" data-command="0627414FR59">
                Voir ma commande
            </button>
            <button class="btn btn-dark rounded-0 p-3">
                Retourner un article
            </button>
        </div>
    </div>
</div>

<div id="0627414FR59" class="row mt-2 d-none">
    <div class="col-12 bg-white mt-2">
        <div class="py-1 d-flex">
            <img class="col-lg-1 col-sm-2 p-0 mr-3"
                 src="/mca-informatique/assets/images/articles/acer-nitro-n50-610/acer_nitroN50-610_2.jpg"
                 alt="acer_nitroN50-610_2">
            <div class="col">
                <h5 class="position-absolute fixed-top ml-3 mb-0">
                    <a href="/mca-informatique/articles/acer-nitro-n50-610">
                        Acer Nitro N50-610
                    </a>
                </h5>
                <p class="lead position-absolute fixed-bottom ml-3 mb-0 price">
                    999.95 €
                </p>
            </div>
            <div class="d-block text-right my-auto">
                <button data-article="17" class="btn btn-primary rounded-0">
                    Suivre mon colis
                </button>
            </div>
        </div>
    </div>

    <div class="col-12 bg-white mt-2">
        <div class="py-1 d-flex">
            <img class="col-lg-1 col-sm-2 p-0 mr-3"
                 src="/mca-informatique/assets/images/articles/souris-logitech/souris_logitech.jpg"
                 alt="souris_logitech">
            <div class="col">
                <h5 class="position-absolute fixed-top ml-3 mb-0">
                    <a href="/mca-informatique/articles/souris-logitech">
                        Souris Logitech
                    </a>
                </h5>
                <p class="lead position-absolute fixed-bottom ml-3 mb-0 price">
                    9.99 €
                </p>
            </div>
            <div class="d-block text-right my-auto">
                <button data-article="12" class="btn btn-primary rounded-0">
                    Suivre mon colis
                </button>
            </div>
        </div>
    </div>
</div>