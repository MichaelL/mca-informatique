<div id="sidebar" class="collapse show d-lg-block jumbotron p-3">
    <div class="list-group list-group-flush" id="list-tab" role="tablist">
        <a class="list-group-item bg-transparent active" id="list-compte"
           data-toggle="list" href="#compte" role="tab"
           aria-controls="compte">
            Mon compte
        </a>
        <a class="list-group-item bg-transparent" id="list-infos"
           data-toggle="list" href="#infos" role="tab"
           aria-controls="infos">
            Informations du compte
        </a>
        <a class="list-group-item bg-transparent" id="list-adresses"
           data-toggle="list" href="#adresses" role="tab"
           aria-controls="adresses">
            Mes adresses
        </a>
<!--        <a class="list-group-item bg-transparent" id="list-comments"-->
<!--           data-toggle="list" href="#comments" role="tab"-->
<!--           aria-controls="comments">-->
<!--            Mes commentaires-->
<!--        </a>-->
        <a class="list-group-item bg-transparent" id="list-commands"
           data-toggle="list" href="#commands" role="tab"
           aria-controls="commands">
            Mes commandes
        </a>
        <a class="list-group-item bg-transparent" href="contact">
            Nous contacter
        </a>
        <a class="mt-5 btn btn-danger" href="<?= WEBROOT ?>logout.php">Me déconnecter</a>
    </div>
</div>