<div class="row">
    <h3 class="col-12 mb-3"> Bienvenue <?= $_SESSION['auth']["user_lastname"].' '.$_SESSION['auth']["user_firstname"] ?> </h3>
    <div class="col-lg-6">
        <div class="jumbotron p-3">
            <p class="lead"> Informations de contact</p>
            <address>
                <a href="mailto: <?= $_SESSION['auth']['user_mail'] ?>"><?= $_SESSION['auth']['user_mail'] ?></a>
                <?php if ($_SESSION['auth']['user_phone']): ?>
                <hr>
                <a href="tel: <?= $_SESSION['auth']['user_phone'] ?>"><?= $_SESSION['auth']['user_phone'] ?></a>
                <hr>
                <?php endif; ?>
            </address>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="jumbotron p-3">
            <p class="lead"> Mes commandes</p>

        </div>
    </div>

    <div class="col-lg-6">
        <div class="jumbotron p-3">
            <p class="lead"> Adresse de livraison</p>
            <?php if ($delivery): ?>
                <p><?= $delivery['delivery_country'] ?></p>
                <p><?= $delivery['delivery_number'] ?>, <?= $delivery['delivery_street'] ?></p>
                <p><?= $delivery['delivery_zip'] ?>, <?= $delivery['delivery_city'] ?> <?= $delivery['delivery_cedex'] ?> </p>
            <?php endif; ?>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="jumbotron p-3">
            <p class="lead"> Adresse de facturation</p>
            <?php if ($billing): ?>
            <p><?= $billing['billing_country'] ?></p>
            <p><?= $billing['billing_number'] ?>, <?= $billing['billing_street'] ?></p>
            <p><?= $billing['billing_zip'] ?>, <?= $billing['billing_city'] ?> <?= $billing['billing_cedex'] ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>