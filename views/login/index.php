<div class="container container-login">

    <form method="post" class="form-row">
        <label for="mail" class="col-12">
            <input id="mail" name="mail" type="email" class="form-control"
                   placeholder="Adresse e-mail" required>
        </label>

        <label for="password" class="col-12">
            <input id="password" name="password" type="password" class="form-control"
                   placeholder="Mot de passe" required>
        </label>
        <div class="col-12 my-4 text-center">
            <a href="<?= WEBROOT ?>register" class="text-light lead">Nouveau client ?</a>
        </div>
        <div class="col-12 text-center">
            <button id="login" type="submit" class="btn btn-mca">Se connecter</button>
        </div>
    </form>

    <div class="text-center">
        <a href="<?= WEBROOT ?>" class="text-light">Mot de passe oublié ?</a>
    </div>
</div>