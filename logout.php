<script>
    if (sessionStorage.getItem('cart')) {
        sessionStorage.removeItem('cart')
    }
    window.location.href = 'login';
</script>
<?php
session_start();
session_unset();
session_destroy();
