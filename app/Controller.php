<?php
abstract class Controller{
    /**
     * Afficher une vue
     *
     * @param string $fichier
     * @param array $data
     * @param string $template
     * @return void
     */
    public function render(string $fichier, array $data = [], string $template = "default"){
        extract($data, EXTR_OVERWRITE);

        // On démarre le buffer de sortie
        ob_start();

        // On génère la vue
        require_once(ROOT.'views/'.str_replace("controller", "", strtolower(get_class($this))).'/'.$fichier.'.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT.'views/templates/'.$template.'.php');
    }

    /**
     * Permet de charger un modèle
     *
     * @param string $model
     * @return void
     */
    public function loadModel(string $model){
        // On va chercher le fichier correspondant au modèle souhaité
        require_once(ROOT.'models/'.$model.'.php');
        
        // On crée une instance de ce modèle. Ainsi "Articles" sera accessible par $this->Articles
        $this->$model = new $model();
    }

    /**
     * @return string
     */
    public function alert()
    {
        if (isset($_SESSION['Alert'])) {
            $type = $_SESSION['Alert']['type'];
            $message = $_SESSION['Alert']['message'];
            unset($_SESSION['Alert']);
            return "<div class='alert alert-$type'>$message</div>";
        }
        return '';
    }

    /**
     * @param string $message
     * @param string $type
     * @return void
     */
    public function setAlert(string $message, $type = "danger")
    {
        $_SESSION['Alert']['message'] = $message;
        $_SESSION['Alert']['type'] = $type;
    }

    /**
     * array['type']        type de bouton [close, a, button, input] (default : a)
     * array['link']        contient le lien du bouton
     * array['name']        contient le texte du bouton
     * array['color']       contient la couleur du bouton [light, outline-primary, mca, warning, danger, success, ...]
     *
     * @param $id
     * @param $message
     * @param array ...$buttons (see above)
     * @return string
     */
    public function modal($id, $message, array ...$buttons)
    {
        $buttonsHTML = [];
        foreach ($buttons[0] as $k => $btn) {
            if (!isset($btn['type'])) {
                $btn['type'] = 'a';
            }
            switch ($btn['type']) {
                case 'close':
                    $buttonsHTML[$k] = "
                    <button type='button' class='btn btn-light' data-dismiss='modal'>
                        Retour
                    </button>
                    ";
                    break;
                case 'a':
                    $buttonsHTML[$k] = "
                    <a href='{$btn['link']}' class='btn btn-{$btn['color']}'>
                        {$btn['name']}
                    </a>
                ";
                    break;
                case 'button':
                    $buttonsHTML[$k] = "
                    <button id='{$btn['link']}' type='submit' class='btn btn-{$btn['color']}'>{$btn['name']}</button>
                ";
                    break;
                case 'input':
                    $buttonsHTML[$k] = "
                    <span class='btn btn-{$btn['color']}'>
                        <input id='{$btn['link']}' name='{$btn['link']}' type='submit' class='btn p-0' value='{$btn['name']}'>
                    </span>
                ";
                    break;
            }
        }

        $HTML = "
         <div class='modal fade' id='$id' data-backdrop='static' tabindex='-1' aria-hidden='true'>
            <div class='modal-dialog'>
                <div class='modal-content bg-dark-mca'>
                    <div class='modal-header'>
                        <h5 class='modal-title text-light'>$message</h5>
                    </div>
                    <div class='modal-footer justify-content-between'>";
                        foreach ($buttonsHTML as $btnHTML) {
                            $HTML .= $btnHTML;
                        }
                    $HTML .= "
                    </div>
                </div>
            </div>
         </div>
        ";
        return $HTML;
    }

    /**
     * @param string $text
     * @return string
     */
    public function transformToUrl(string $text) {
        $utf8 = [
            '#à|á|â|ã|ä|å#' => 'a',
            '#À|Á|Â|Ã|Ä|Å#' => 'A',
            '#ì|í|î|ï#' => 'i',
            '#Ì|Í|Î|Ï#' => 'I',
            '#è|é|ê|ë#' => 'e',
            '#È|É|Ê|Ë#' => 'E',
            '#ð|ò|ó|ô|õ|ö#' => 'o',
            '#Ò|Ó|Ô|Õ|Ö#' => 'O',
            '#ù|ú|û|ü#' => 'u',
            '#Ù|Ú|Û|Ü#' => 'U',
            '#ý|ÿ#' => 'y',
            '#Ý#' => 'Y',
            '#ç#' => 'c',
            '#Ç#' => 'C',
            '#ñ#' => 'n',
            '#Ñ#' => 'N',
            '# #' => '-'
        ];
        return strtolower(preg_replace(array_keys($utf8), array_values($utf8), $text));
    }

    /**
     * @param string $text
     * @return string
     */
    public function upperSpecialChar(string $text)
    {
        $utf8 = [
            '#à#' => 'À',
            '#á#' => 'Á',
            '#â#' => 'Â',
            '#ã#' => 'Ã',
            '#ä#' => 'Ä',
            '#å#' => 'Å',
            '#ì#' => 'Ì',
            '#í#' => 'Í',
            '#î#' => 'Î',
            '#ï#' => 'Ï',
            '#è#' => 'È',
            '#é#' => 'É',
            '#ê#' => 'Ê',
            '#ë#' => 'Ë',
            '#ò#' => 'Ò',
            '#ó#' => 'Ó',
            '#ô#' => 'Ô',
            '#õ#' => 'Õ',
            '#ö#' => 'Ö',
            '#ù#' => 'Ù',
            '#ú#' => 'Ú',
            '#û#' => 'Û',
            '#ü#' => 'Ü',
            '#ç#' => 'Ç'
        ];
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}