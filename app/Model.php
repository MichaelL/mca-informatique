<?php
abstract class Model{
    // Propriété qui contiendra l'instance de la connexion
    protected $_connexion;

    // Propriétés permettant de personnaliser les requêtes
    public $table;

    /**
     * Fonction d'initialisation de la base de données
     *
     * @return void
     */
    public function getConnection(){
        // On supprime la connexion précédente
        $this->_connexion = null;

        // On essaie de se connecter à la base
        try{
            $this->_connexion = new PDO('mysql:host=localhost;dbname=mca', 'root', '');
            $this->_connexion->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Erreur de connexion : " . $exception->getMessage();
        }
    }

    /**
     * Méthode permettant d'obtenir un enregistrement de la table choisie en fonction d'un id
     *
     * @param string $_id
     * @param int $id
     * @return void
     */
    public function getById(string $_id, int $id){
        $sql = "SELECT * FROM ".$this->table." WHERE $_id=".$id;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Méthode permettant d'obtenir tous les enregistrements de la table choisie
     *
     * @param string $select
     * @param string $condition
     * @return void
     */
    public function getAll($condition = '', $select = '*'){
        $sql = "SELECT ".$select." FROM ".$this->table." ".$condition;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}