<?php

use PHPMailer\PHPMailer\PHPMailer;

class ContactController extends Controller
{
    /**
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function index()
    {
        if (isset($_POST['subject'], $_POST['mail'])) {

            require 'PHPMailer/src/PHPMailer.php';
            require 'PHPMailer/src/Exception.php';
            require 'PHPMailer/src/SMTP.php';

            $mail = new PHPMailer;

            // MA BOITE MAIL
            $to = "michael.lemay@lilo.org";

            $mail->isSMTP();
            $mail->Host = 'mail.lilo.org';
            $mail->SMTPAuth = true ;
            $mail->Port = 587;
            $mail->Username = $to;
            $mail->Password = 'mickey3D';
            $mail->SMTPSecure = 'TLS';

            $mail->From =  $to;
            $mail->FromName = $_POST['mail'];
            $mail->addAddress($to, 'michael');

            $mail->isHTML(true);

            $mail->Subject = $_POST['subject'];
            $mail->Body = $_POST['message'];

            if (!$mail->send()) {
                $this->setAlert($mail->ErrorInfo);
            } else {
                $this->setAlert("Votre message est en route !", "success");
            }
        }
        $this->render('index', [], 'form');
    }
}