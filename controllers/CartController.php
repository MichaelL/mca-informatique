<?php


class CartController extends Controller
{
    /**
     * @throws JsonException
     */
    public function index()
    {
        if (isset($_POST['cart'])) {
            $brand = 'Mon panier';
            $articles = [];
            if (!empty($_POST['cart'])) {
                $this->loadModel('Articles');
                $this->loadModel('Images');

                $cart = json_decode($_POST['cart'], true, 512, JSON_THROW_ON_ERROR);
                $cart = implode(', ', $cart);
                $articles = $this->Articles->getArticles('article_id IN ('.$cart.')');
            }

            $this->render('index', compact('articles', 'brand'));
        } else {
            header('Location: '.WEBROOT);
        }
    }
}