<?php


class AccountController extends Controller
{
    public function index()
    {
        if (isset($_SESSION['auth'])) {
            $brand = 'Mon compte';
            $this->loadModel('Users');
            $this->loadModel('Billing');
            $this->loadModel('Delivery');
            $this->loadModel('Comments');
            $this->loadModel('Articles');

            $user = $this->Users->getUserMail($_SESSION['auth']['user_mail']);
            $billing = $this->Billing->getById('billing_user', $user['user_id']);
            $delivery = $this->Delivery->getById('delivery_user', $user['user_id']);

            if (isset($_POST['user'])) {
                if (strlen($_POST['phone']) !== 10) {
                    $this->setAlert('Numéro incorrect');
                } elseif ($_POST['password'] !== '' && strlen($_POST['password']) < 8) {
                    $this->setAlert('Le <b>Mot de passe</b> doit contenir <b>8 caractères minimum</b>');
                } else {
                    $password = ($_POST['password'] !== '')? password_hash($_POST['password'], PASSWORD_BCRYPT) : $user['user_password'];
                    if ($_POST['mail'] !== $user['user_mail'] || $password !== $user['user_password']) {
                        header('Location: '.WEBROOT.'logout.php');
                    } else {
                        $_SESSION['auth']["user_lastname"] = $_POST['lastname'];
                        $_SESSION['auth']["user_firstname"] = $_POST['firstname'];
                        $_SESSION['auth']["user_phone"] = $_POST['phone'];
                    }
                    $this->setAlert('Modifications enregistrées', 'success');
                    $this->Users->update($user['user_id'], $_POST['lastname'], $_POST['firstname'], $_POST['mail'], $password, $_POST['phone']);
                }

                if (isset($_FILES['image']) && $_FILES['image']['size'] !== 0) {
                    $file_name = $_FILES['image']['name'];
                    $file_extension = pathinfo($file_name,PATHINFO_EXTENSION);
                    // le répertoire où le fichier va être placé
                    $target_dir = IMAGES. 'users/' .$user['user_id'];
                    // si le répertoire n'existe pas, le créer
                    if (!is_dir($target_dir)) {
                        mkdir($target_dir, 0777, true);
                    }
                    // le chemin du fichier à télécharger
                    $target_file = $target_dir .'/'. basename($file_name);
                    // Vérifie si le fichier est une image
                    $check = getimagesize($_FILES["image"]["tmp_name"]);

                    if($check === false) {
                        $this->setAlert('Format non autorisé');
                    }
                    // Format jpg/jpeg uniquement
                    elseif (!in_array($file_extension, ["jpg", "jpeg", "JPG", "JPEG"])) {
                        $this->setAlert('Format non autorisé : <b>.jpg uniquement</b>');
                    }
                    // Si le fichier dépasse 500 Ko
                    elseif ($_FILES["image"]["size"] > 500000) {
                        $this->setAlert('Ce fichier est trop volumineux : <b>500 Ko</b> maximum autorisé');
                    } else {
                        // Si le nom du fichier existe
                        if (file_exists($target_file)) {
                            unlink($target_file);
                            move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                        } else {
                            if (!empty($_SESSION['auth']['user_picture']) && file_exists($target_dir.'/'.$_SESSION['auth']['user_picture'])) {
                                unlink($target_dir . '/' . $_SESSION['auth']['user_picture']);
                            }
                            move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                            $this->Users->updatePicture((int)$user['user_id'], $file_name);
                            $_SESSION['auth']['user_picture'] = $file_name;
                        }
                        if ($_POST['firstname'] === $user['user_firstname']
                            && $_POST['lastname'] === $user['user_lastname']
                            && $_POST['phone'] === $user['user_phone']) {
                            $this->setAlert('<b>' . $file_name . '</b> téléchargé avec succès', 'success');
                        } else {
                            $this->setAlert('Modifications enregistrées', 'success');
                        }
                    }
                }
            }

            if (isset($_POST['billing'])) {
                if ($billing) {
                    $this->Billing->update($billing['billing_id'], strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                        $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                } else {
                    $this->Billing->create(strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                        $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                }
                header('Location:' . WEBROOT . 'account');
            }

            if (isset($_POST['delivery'])) {
                if ($delivery) {
                    $this->Delivery->update($delivery['delivery_id'], strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                        $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                } else {
                    $this->Delivery->create(strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                        $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                }
                if (isset($_POST['billing-and-delivery'])) {
                    if ($billing) {
                        $this->Billing->update($delivery['delivery_id'], strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                            $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                    } else {
                        $this->Billing->create(strtoupper($_POST['country']), (int)$_POST['number'], $_POST['street'],
                            $_POST['city'], $_POST['cedex'], (int)$_POST['zip'], (int)$_SESSION['auth']['user_id']);
                    }
                }
                header('Location:' . WEBROOT . 'account');
            }

            $this->render('index', compact('user', 'billing', 'delivery', 'brand'),'account');
        } else {
            header('Location:' . WEBROOT);
        }
    }
}