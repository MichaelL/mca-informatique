<?php

class LoginController extends Controller
{
    public function index()
    {
        if (!isset($_SESSION['auth'])) {
            $brand = "Connexion";
            // Si le formulaire est envoyé
            if (isset($_POST['mail'], $_POST['password'])) {

                // On instancie le modèle "Users"
                $this->loadModel('Users');

                // On stocke les données de l'utilisateur dans $User
                $user = $this->Users->getUserMail($_POST['mail']);

                // Si l'utilisateur existe et si son password est valide
                if ($user) {

                    if (password_verify($_POST['password'], $user['user_password'])) {
                        // on stock ses infos dans la session
                        $_SESSION['auth'] = $user;
                        if ($_SESSION['auth']['user_rule'] === "2") {
                            header('Location: ' . WEBROOT . 'admin');
                        } else {
                            header('Location: ' . WEBROOT);
                        }
                    } else {
                        $this->setAlert('<b>Mot de passe</b> incorrect');
                    }

                } else {
                    $this->setAlert('<b>Email</b> incorrect');
                }
            }

            // On envoie les données à la vue index
            $this->render('index', compact('brand'), 'form');
        } else if ($_SESSION['auth']['user_rule'] === "2") {
            header('Location:' . WEBROOT . 'admin');
        } else {
            header('Location:' . WEBROOT);
        }
    }
}