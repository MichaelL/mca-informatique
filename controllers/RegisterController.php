<?php

class RegisterController extends Controller
{
    public function index()
    {
        $brand = 'Inscription';
        if (!isset($_SESSION['auth'])) {
            $this->loadModel('Users');

            if (isset($_POST['firstname'], $_POST['lastname'], $_POST['mail'], $_POST['password'])) {
                $user = $this->Users->getUserMail($_POST['mail']);

                if ($user) {
                    $this->setAlert('Cette <b>Adresse Email</b> est <b>déjà utilisé');
                } elseif (strlen($_POST['password']) < 8) {
                    $this->setAlert('Le <b>Mot de passe</b> doit contenir <b>8 caractères minimum</b>');
                } else {
                    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                    $this->Users->create($_POST['lastname'], $_POST['firstname'], $_POST['mail'], $password, 1);
                    $this->setAlert('Vous êtes désormais enregistré, veuillez <a href="'.WEBROOT.'login">vous connecter</a>', 'success');
                }
            }

            $this->render('index', compact('brand'),'form');
        } else if ($_SESSION['auth']['user_rules'] === "2") {
            header('Location:' . WEBROOT . 'admin');
        } else {
            header('Location:' . WEBROOT);
        }
    }
}