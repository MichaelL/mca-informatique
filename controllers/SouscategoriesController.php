<?php

class SouscategoriesController extends Controller
{
    public function index($url = '')
    {
        if ($url) {
            $this->loadModel('Subcat');
            $this->loadModel('Articles');
            $this->loadModel('Images');

            $subcat = $this->Subcat->getSubcatUrl($url);
            $brand = $subcat['subcat_title'];

            $articles = $this->Articles->getArticles('article_subcat='.$subcat['subcat_id']);

            $this->render('index', compact('articles','brand'));
        } else {
            header('Location: '.WEBROOT);
        }
    }
}