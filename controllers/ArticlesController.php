<?php

class ArticlesController extends Controller
{
    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index($url=''){

        $this->loadModel('Articles');
        if (!$url) {
            // On stocke la liste des articles dans $articles
            $articles = $this->Articles->getArticles();

            // On envoie les données à la vue index
            $this->render('index', compact('articles'));
        } else {

            $article = $this->Articles->getArticleUrl($url);
            $brand = $article['article_title'];
            if ($article) {
                $this->loadModel('Users');
                $this->loadModel('Categories');
                $this->loadModel('Subcat');
                $this->loadModel('Images');

                $article['images'] = $this->Images->getAll('WHERE image_article=' . $article['article_id']);
                $subcat = $this->Subcat->getById('subcat_id', $article['article_subcat']);
                $article['subcat_title'] = $subcat['subcat_title'];
                $article['subcat_url'] = $subcat['subcat_url'];
                $category = $this->Categories->getById('category_id', $subcat['subcat_category']);
                $article['category_title'] = $category['category_title'];
                $article['category_url'] = $category['category_url'];
            } else {
                header('Location: '.WEBROOT);
            }

            $this->render('article', compact('article', 'brand'));
        }
    }

}