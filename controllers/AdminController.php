<?php

class AdminController extends Controller
{
    /**
     * @return void
     */
    public function index()
    {
        $brand = 'Admin';
        // On instancie le modèle "Articles"
        $this->loadModel('Articles');
        $this->loadModel('Images');
        $this->loadModel('Categories');
        $this->loadModel('Subcat');
        $this->loadModel('Rules');
        $this->loadModel('Users');

        // On stocke la liste des articles dans $articles
        $articles = $this->Articles->getArticles('', 'DESC');
        $categories = $this->Categories->getAll();
        $rules = $this->Rules->getAll('ORDER BY rule_title');

        foreach ($categories as $category) {
            $subcat[] = $this->Subcat->getAll('WHERE subcat_category=' . $category['category_id']);
        }

        foreach ($rules as $rule) {
            $users[] = $this->Users->getAll('WHERE user_rule=' . $rule['rule_id']);
        }

        if (isset($_POST['rule'])) {
            $this->Users->updateRule($_POST['id'], $_POST['rule']);
            header('Location:' . WEBROOT . 'admin');
        }

        // On envoie les données à la vue index
        $this->render('index', compact('articles', 'categories', 'subcat', 'rules', 'users', 'brand'));
    }

    /**
     * @param $url
     * @return void
     */
    public function subcat_create($url)
    {
        if ($url) {
            $brand = 'Créer une sous-catégorie';

            $this->loadModel('Categories');
            // On stocke la ligne qui possède le $title que l'on récupère dans l'URL
            $category = $this->Categories->getCategoryUrl($url);
            if ($category) {
                if (isset($_POST['title'], $_POST['description'])) {
                    $subcat_url = $this->transformToUrl($_POST['title']);

                    // On instancie les modèles "Subcat" & "Catégories"
                    $this->loadModel('Subcat');

                    $subcat = $this->Subcat->getSubcatUrl($url);

                    if (!$subcat) {
                        $this->Subcat->create($_POST['title'], $_POST['description'], $category['category_id'], $subcat_url);
                    } else {
                        $this->setAlert('Cette sous-catégorie existe déjà');
                    }
                    $this->setAlert('Sous-catégorie créée', "success");
                }

                $this->render('subcat_create', compact('category', 'brand'));
            } else {
                $this->setAlert('Cette catégorie n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }
        } else {
            $this->setAlert('Aucune catégorie trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function subcat_update($url)
    {
        if ($url) {
            $brand = 'Modifier une sous-catégorie';

            $this->loadModel('Subcat');
            $this->loadModel('Categories');

            $subcat = $this->Subcat->getSubcatUrl($url);
            $categories = $this->Categories->getAll();

            if ($subcat) {
                if (isset($_POST['title'], $_POST['description'], $_POST['category'])) {
                    $subcat_url = $this->transformToUrl($_POST['title']);

                    $this->Subcat->update($subcat['subcat_id'], $_POST['title'], $_POST['description'], $_POST['category'], $subcat_url);
                    $this->setAlert('Sous-catégorie mis à jour', "success");
                }
            } else {
                $this->setAlert('Cette sous-catégorie n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

            $this->render('subcat_update', compact('subcat', 'categories', 'brand'));
        } else {
            $this->setAlert('Aucune sous-catégorie trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function subcat_delete($id)
    {
        if ($id) {
            $this->loadModel('Subcat');
            $subcat = $this->Subcat->getById('subcat_id', $id);

            if ($subcat) {
                $this->Subcat->delete($id);
            } else {
                $this->setAlert('Cette sous-catégorie n\'existe pas');
            }

        } else {
            $this->setAlert('Aucune sous-catégorie trouvé');
        }
        header('Location:' . WEBROOT . 'admin');
    }

    public function rule_create()
    {
        $brand = 'Créer un rôle';

        if (isset($_POST['title'], $_POST['description'])) {
            $this->loadModel('Rules');

            $rule = $this->Rules->getRuleTitle($_POST['title']);

            $rule_title = $this->transformToUrl($_POST['title']);

            if (!$rule) {
                $this->Rules->create($rule_title, $_POST['description']);
                $this->setAlert('Rôle créé', "success");
            } else {
                $this->setAlert('Ce rôle existe déjà');
            }

        }
        $this->render('rule_create', compact('brand'));
    }

    public function rule_update($url)
    {
        if ($url) {
            $brand = 'Modifier un rôle';

            $this->loadModel('Rules');

            $rule = $this->Rules->getRuleTitle($url);

            if ($rule) {
                if (isset($_POST['title'], $_POST['description'])) {
                    $rule_title = $this->transformToUrl($_POST['title']);

                    $this->Subcat->update($rule['rule_id'], $rule_title, $_POST['description']);
                    $this->setAlert('Rôle mis à jour', "success");
                }
            } else {
                $this->setAlert('Ce rôle n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

            $this->render('rule_update', compact('rule', 'brand'));
        } else {
            $this->setAlert('Aucune sous-catégorie trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function rule_delete($id)
    {
        if ($id) {
            $this->loadModel('Rules');
            $user = $this->Rules->getById('rule_id', $id);

            if ($user) {
                $this->Rules->delete($id);
                header('Location:' . WEBROOT . 'admin');
            } else {
                $this->setAlert('Ce rôle n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

        } else {
            $this->setAlert('Aucune utilisateur trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function user_delete($id)
    {
        if ($id) {
            $this->loadModel('Users');
            $user = $this->Users->getById('user_id', $id);

            if ($user) {
                $this->Users->delete($id);
                header('Location:' . WEBROOT . 'admin');
            } else {
                $this->setAlert('Cette utilisateur n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

        } else {
            $this->setAlert('Aucune utilisateur trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function article_create()
    {
        $brand = 'Créer un article';

        $this->loadModel('Categories');
        $this->loadModel('Subcat');

        $categories = $this->Categories->getAll();

        foreach ($categories as $k => $category) {
            $subcat[] = $this->Subcat->getAll('WHERE subcat_category=' . $category['category_id']);
        }

        if (isset($_POST['title'])) {
            $this->loadModel('Articles');

            $url = $this->transformToUrl($_POST['title']);

            $article = $this->Articles->getArticleUrl($url);

            if ($article) {
                $this->setAlert('Cette article existe déjà');
            } else {
                if (isset($_POST['dispo'])) {
                    $dispo = true;
                } else {
                    $dispo = false;
                }

                if (isset($_FILES['image']) && $_FILES['image']['size'] !== 0) {
                    $file_name = $_FILES['image']['name'];
                    $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
                    // le répertoire où le fichier va être placé
                    $target_dir = IMAGES . 'articles/' . $url;

                    // le chemin du fichier à télécharger
                    $target_file = $target_dir . '/' . basename($file_name);
                    // Vérifie si le fichier est une image
                    $check = getimagesize($_FILES["image"]["tmp_name"]);

                    if ($check === false) {
                        $this->setAlert('Format non autorisé');
                    } // Taille 374x374 uniquement
                    elseif ($check[0] !== 374 && $check[1] !== 374) {
                        $this->setAlert('Taille non autorisé : <b>374x374</b> uniquement');
                    } // Format jpg/jpeg uniquement
                    elseif (!in_array($file_extension, ["jpg", "jpeg", "JPG", "JPEG"])) {
                        $this->setAlert('Format non autorisé : <b>.jpg uniquement</b>');
                    } // Si le fichier dépasse 500 Ko
                    elseif ($_FILES["image"]["size"] > 500000) {
                        $this->setAlert('Ce fichier est trop volumineux : <b>500 Ko</b> maximum autorisé');
                    } else {
                        // si le répertoire n'existe pas, le créer
                        if (!is_dir($target_dir)) {
                            mkdir($target_dir, 0777, true);
                        }
                        $this->loadModel('Images');

                        $this->Articles->create($_POST['title'], $_POST['description'], (float)$_POST['price'], $dispo,
                            $_POST['date'], (int)$_POST['promo'], (int)$_POST['subcat'], $url);

                        $article = $this->Articles->getArticleUrl($url);

                        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
                        $this->Images->create($file_name, $article['article_id']);
                    }
                }
            }
            header('Location: '.WEBROOT. 'admin/article_update/'.$url);
        }
        $this->render('article_create', compact('categories', 'subcat', 'brand'));
    }

    public function article_update($url)
    {
        if ($url) {
            $brand = 'Modifier un article';

            $this->loadModel('Articles');

            $article = $this->Articles->getArticleUrl($url);

            if ($article) {

                $this->loadModel('Categories');
                $this->loadModel('Subcat');
                $this->loadModel('Images');

                $article['images'] = $this->Images->getAll('WHERE image_article=' . $article['article_id']);

                $categories = $this->Categories->getAll();

                foreach ($categories as $category) {
                    $subcat[] = $this->Subcat->getAll('WHERE subcat_category=' . $category['category_id']);
                }

                if (isset($_POST['title'])) {
                    $article_url = $this->transformToUrl($_POST['title']);

                    if (isset($_POST['dispo'])) {
                        $dispo = true;
                    } else {
                        $dispo = false;
                    }
                    $this->Articles->update($article['article_id'], $_POST['title'], $_POST['description'],
                        (float)$_POST['price'], $dispo, $_POST['date'], (int)$_POST['promo'], (int)$_POST['subcat'],
                        $article_url);

                    // le répertoire où le fichier va être placé
                    $target_dir = IMAGES . 'articles/' . $article_url;

                    // si le répertoire n'existe pas, le créer
                    if (!is_dir($target_dir)) {
                        // l'ancien répertoire
                        $old_dir = IMAGES . 'articles/' . $article['article_url'];
                        $dir = opendir( $old_dir );  // ouvre le répertoire
                        $files = readdir( $dir );

                        rename("$old_dir.$files", "$target_dir.$files");
                        closedir( $dir );
                    }

                    if (isset($_FILES['image']) && $_FILES['image']['size'] !== 0) {
                        if (count($article['images']) >= 4) {
                            $this->setAlert('Nombre d\'images maximum atteint');
                        } else {
                            $file_name = $_FILES['image']['name'];
                            $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
                            // le chemin du fichier à télécharger
                            $target_file = $target_dir . '/' . basename($file_name);
                            // Vérifie si le fichier est une image
                            $check = getimagesize($_FILES["image"]["tmp_name"]);

                            if ($check === false) {
                                $this->setAlert('Format non autorisé');
                            } // Taille d'image 374x374
                            elseif ($check[0] !== 374 && $check[1] !== 374) {
                                $this->setAlert('Taille non autorisé : <b>374x374</b> uniquement');
                            } // Format jpg/jpeg uniquement
                            elseif (!in_array($file_extension, ["jpg", "jpeg", "JPG", "JPEG"])) {
                                $this->setAlert('Format non autorisé : <b>.jpg uniquement</b>');
                            } // Si le fichier dépasse 500 Ko
                            elseif ($_FILES["image"]["size"] > 500000) {
                                $this->setAlert('Ce fichier est trop volumineux : <b>500 Ko</b> maximum autorisé');
                            } else {
                                $this->loadModel('Images');
                                if (file_exists($target_file)) {
                                    unlink($target_file);
                                    move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                                }

                                move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
                                $this->Images->create($file_name, $article['article_id']);
                                header('Location:' . WEBROOT . 'admin/article_update/'.$url);
                            }
                        }
                    }
                    $this->setAlert('Article mis à jour', "success");
                }
            } else {
                $this->setAlert('Cette article n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

            $this->render('article_update', compact('article', 'categories', 'subcat', 'brand'));
        } else {
            $this->setAlert('Aucun article trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }

    public function article_delete($id)
    {
        if ($id) {
            $this->loadModel('Articles');
            $article = $this->Articles->getById('article_id', $id);

            if ($article) {
                $this->loadModel('Images');
                $this->Images->deleteImagesArticle($id);
                $this->Articles->delete($id);
                // nom du répertoire
                $dir_name = IMAGES . 'articles/' . $article['article_url'];
                if (is_dir($dir_name)) {
                    // ouvre le répertoire
                    $dir = opendir($dir_name);
                    $files = readdir($dir);
                    // supprime chaque fichier du répertoire
                    while ($files = readdir($dir)) {
                        unlink("$dir_name/$files");
                    }
                    // ferme le répertoire
                    closedir($dir);
                    // supprime le répertoire
                    rmdir($dir_name);
                }

            } else {
                $this->setAlert('Cette article n\'existe pas');
            }

        } else {
            $this->setAlert('Aucun article trouvé');
        }
        header('Location:' . WEBROOT . 'admin');
    }

    public function image_delete($id)
    {
        if ($id) {
            $this->loadModel('Articles');
            $this->loadModel('Images');
            $image = $this->Images->getById('image_id', $id);
            $images = $this->Images->getAll('WHERE image_article='.$image['image_article']);
            $url = $this->Articles->getById('article_id', $image['image_article'])['article_url'];

            if ($image) {
                if (count($images) > 1) {
                    $file_name = $image['image_name'];
                    $target_dir = IMAGES . 'articles/' . $image['image_article'];
                    $target_file = $target_dir . '/' . basename($file_name);

                    if (file_exists($target_file)) {
                        unlink($target_file);
                    }
                    $this->Images->delete($id);
                } else {
                    $this->setAlert('Veuillez télécharger une image avant de supprimer celle-ci');
                }
                header('Location:' . WEBROOT . 'admin/article_update/'.$url);
            } else {
                $this->setAlert('Cette image n\'existe pas');
                header('Location:' . WEBROOT . 'admin');
            }

        } else {
            $this->setAlert('Aucune image trouvé');
            header('Location:' . WEBROOT . 'admin');
        }
    }
}