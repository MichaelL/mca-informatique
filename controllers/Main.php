<?php

class Main extends Controller
{
    /**
     * @return void
     */
    public function index(){
        // On instancie le modèle "Articles"
        $this->loadModel('Articles');

        // On stocke la liste des articles dans $articles
        $articles['promo'] = $this->Articles->getArticles('article_promo!=0', 'DESC', '4');
        $articles['new'] = $this->Articles->getArticles('article_promo=0', 'DESC', '4');

        // On envoie les données à la vue index
        $this->render('index', compact('articles'));
    }
}