<?php


class Comments extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "comments";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    public function getUserComments($user)
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE comments_user=?
        ORDER BY comments_id DESC";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$user]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getArticleComments($article)
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE comments_article=?
        ORDER BY comments_id DESC";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$article]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    // C'est ici que l'on va gérer notre CRUD
    public function create($title, $comment, int $user, int $article)
    {
        $sql = "INSERT INTO " . $this->table . " SET comments_title=?, comments_description=?,
        comments_user=?, comments_article=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $comment, $user, $article]);
    }
}