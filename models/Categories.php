<?php


class Categories extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "categories";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    public function getCategoryUrl($url)
    {
        $sql = "SELECT category_id, category_title, category_url, category_description, category_color FROM ".$this->table." WHERE category_url=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$url]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    // C'est ici que l'on va gérer notre CRUD
}