<?php


class Billing extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "billing";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @param $country
     * @param int $number
     * @param $street
     * @param $city
     * @param $cedex
     * @param $zip
     * @param int $user
     */
    public function create($country, int $number, $street, $city, $cedex, $zip, int $user)
    {
        $sql = "INSERT INTO ".$this->table." SET billing_country=?, billing_number=?, billing_street=?, billing_city=?,
        billing_cedex=?, billing_zip=?, billing_user=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$country, $number, $street, $city, $cedex, $zip, $user]);
    }

    /**
     * @param int $id
     * @param $country
     * @param int $number
     * @param $street
     * @param $city
     * @param $cedex
     * @param $zip
     * @param int $user
     */
    public function update(int $id, $country, int $number, $street, $city, $cedex, $zip, int $user)
    {
        $sql = "UPDATE ".$this->table." SET billing_country=?, billing_number=?, billing_street=?, billing_city=?,
        billing_cedex=?, billing_zip=?, billing_user=?
        WHERE billing_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$country, $number, $street, $city, $cedex, $zip, $user, $id]);
    }
}