<?php


class Users extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "users";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    // C'est ici que l'on va gérer notre CRUD
    /**
     * Retourne un utilisateur en fonction de son email
     *
     * @param string $mail
     * @return mixed
     */
    public function getUserMail(string $mail){
        $sql = "SELECT user_id, user_lastname, user_firstname, user_mail, user_password, user_phone, user_picture, user_rule FROM ".$this->table." WHERE user_mail=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$mail]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $lastname
     * @param $firstname
     * @param $mail
     * @param $password
     * @param int $rule
     */
    public function create($lastname, $firstname, $mail, $password, int $rule)
    {
        $sql = "INSERT INTO ".$this->table." SET user_lastname=?, user_firstname=?, user_mail=?, user_password=?, user_rule=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$lastname, $firstname, $mail, $password, $rule]);
    }

    public function updateRule(int $id, int $rule)
    {
        $sql = "UPDATE ".$this->table." SET user_rule=?
        WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$rule, $id]);
    }

    public function updatePicture(int $id, $picture)
    {
        $sql = "UPDATE ".$this->table." SET user_picture=?
        WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$picture, $id]);
    }

    public function update(int $id, $lastname, $firstname, $mail, $password, $phone)
    {
        $sql = "UPDATE ".$this->table." SET user_lastname=?, user_firstname=?, user_mail=?, user_password=?,
        user_phone=?
        WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$lastname, $firstname, $mail, $password, $phone, $id]);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }
}