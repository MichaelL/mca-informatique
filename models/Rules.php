<?php


class Rules extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "rules";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    public function getRuleTitle(string $title)
    {
        $sql = "SELECT rule_id, rule_title, rule_description FROM ".$this->table." WHERE rule_title=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    // C'est ici que l'on va gérer notre CRUD
    public function create($title, $description)
    {
        $sql = "INSERT INTO ".$this->table." SET rule_title=?, rule_description=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $description]);
    }

    /**
     * @param int $id
     * @param $title
     * @param $description
     */
    public function update(int $id, $title, $description)
    {
        $sql = "UPDATE ".$this->table." SET rule_title=?, rule_description=?
        WHERE rule_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $description, $id]);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE rule_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }
}