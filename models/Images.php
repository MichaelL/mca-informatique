<?php


class Images extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "images";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    // C'est ici que l'on va gérer notre CRUD
    public function create($name, $article)
    {
        // Créer une requête
        $sql = "INSERT INTO ".$this->table." SET image_name=?, image_article=?";
        // Préparer la requête
        $query = $this->_connexion->prepare($sql);
        // Exécuter la requête
        $query->execute([$name, $article]);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE image_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }

    /**
     * @param int $id
     */
    public function deleteImagesArticle(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE image_article=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }
}