<?php


class Delivery extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "delivery";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @param $country
     * @param int $number
     * @param $street
     * @param $city
     * @param $cedex
     * @param $zip
     * @param int $user
     */
    public function create($country, int $number, $street, $city, $cedex, $zip, int $user)
    {
        $sql = "INSERT INTO ".$this->table." SET delivery_country=?, delivery_number=?, delivery_street=?,
        delivery_city=?, delivery_cedex=?, delivery_zip=?, delivery_user=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$country, $number, $street, $city, $cedex, $zip, $user]);
    }

    /**
     * @param int $id
     * @param $country
     * @param int $number
     * @param $street
     * @param $city
     * @param $cedex
     * @param $zip
     * @param int $user
     */
    public function update(int $id, $country, int $number, $street, $city, $cedex, $zip, int $user)
    {
        $sql = "UPDATE ".$this->table." SET delivery_country=?, delivery_number=?, delivery_street=?, delivery_city=?,
        delivery_cedex=?, delivery_zip=?, delivery_user=?
        WHERE delivery_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$country, $number, $street, $city, $cedex, $zip, $user, $id]);
    }
}