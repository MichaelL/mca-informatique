<?php


class Articles extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "articles";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @param string $url
     * @return mixed
     */
    public function getArticleUrl(string $url)
    {
        $sql = "SELECT article_id, article_title, article_url, article_description, article_price, article_dispo,
        article_date, article_promo, article_subcat FROM ".$this->table." WHERE article_url=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$url]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getArticles($and = '', $desc = 'ASC', $limit = '')
    {
        if ($and) {
            $and = "AND ".$and;
        }
        if ($limit) {
            $limit = "LIMIT ".$limit;
        }

        $sql = "SELECT article_id, article_title, article_url, article_description,
       article_price, article_dispo, article_date, article_promo, article_subcat,
       sc.subcat_title, sc.subcat_url,
       c.category_title, c.category_url,
       img.image_id, img.image_name
        FROM ".$this->table.",
            subcat sc,
            categories c,
            images img
               WHERE sc.subcat_id=article_subcat
               AND sc.subcat_category=c.category_id
               AND article_id=img.image_article
               ".$and."
        GROUP BY article_id "
            .$desc." "
            .$limit;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    
    // C'est ici que l'on va gérer notre CRUD

    /**
     * @param $title
     * @param $description
     * @param float $price
     * @param bool $dispo
     * @param $date
     * @param int $promo
     * @param int $subcat
     * @param $url
     */
    public function create($title, $description, float $price, bool $dispo, $date, int $promo, int $subcat, $url)
    {
        // Créer une requête
        $sql = "INSERT INTO " . $this->table . " SET article_title=?, article_description=?, article_price=?,
        article_dispo=?, article_date=?, article_promo=?, article_subcat=?, article_url=?";
        // Préparer la requête
        $query = $this->_connexion->prepare($sql);
        // Exécuter la requête
        $query->execute([$title, $description, $price, $dispo, $date, $promo, $subcat, $url]);
    }

    public function update(int $id,$title, $description, float $price, bool $dispo, $date, int $promo, int $subcat, $url)
    {
        $sql = "UPDATE " . $this->table . " SET article_title=?, article_description=?, article_price=?,
        article_dispo=?, article_date=?, article_promo=?, article_subcat=?, article_url=?
        WHERE article_id=?";
        // Préparer la requête
        $query = $this->_connexion->prepare($sql);
        // Exécuter la requête
        $query->execute([$title, $description, $price, $dispo, $date, $promo, $subcat, $url, $id]);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE article_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }
}