<?php


class Subcat extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "subcat";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    public function getSubcatUrl(string $url)
    {
        $sql = "SELECT subcat_id, subcat_title, subcat_url, subcat_description, subcat_category FROM ".$this->table." WHERE subcat_url=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$url]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }
    // CRUD

    // On va créer une fonction pour créer des sous-catégories (Create)
    /**
     * @param $title
     * @param $description
     * @param $category
     * @param $url
     */
    public function create($title, $description, $category, $url)
    {
        // Créer une requête
        $sql = "INSERT INTO ".$this->table." SET subcat_title=?, subcat_description=?, subcat_category=?, subcat_url=?";
        // Préparer la requête
        $query = $this->_connexion->prepare($sql);
        // Exécuter la requête
        $query->execute([$title, $description, $category, $url]);
    }

    /**
     * @param int $id
     * @param $title
     * @param $description
     * @param $category
     * @param $url
     */
    public function update(int $id, $title, $description, $category, $url)
    {
        $sql = "UPDATE ".$this->table." SET subcat_title=?, subcat_description=?, subcat_category=?, subcat_url=?
        WHERE subcat_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $description, $category, $url, $id]);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE subcat_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }
}