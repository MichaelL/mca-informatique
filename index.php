<?php
session_start();

// On génère une constante contenant le chemin vers la racine publique du projet
define('ROOT', str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']));
define('DIRECTORY', dirname(__FILE__, 2));
define('WEBROOT', "/mca-informatique/");
define('IMAGES', DIRECTORY.WEBROOT.'assets/images/');

if (false !== strpos($_SERVER['REQUEST_URI'], "/mca-informatique/admin")) {
    (isset($_SESSION['auth']) && $_SESSION['auth']['user_rule'] === "2") ?: header('Location: '.WEBROOT.'login');
}

// On appelle le modèle et le contrôleur principaux
require_once(ROOT.'app/Model.php');
require_once(ROOT.'app/Controller.php');

// On sépare les paramètres et on les met dans le tableau $params
$params = explode('/', $_GET['p']);

// Si au moins 1 paramètre existe
if($params[0] !== ""){
    // On sauvegarde le 1er paramètre dans $controller en mettant sa 1ère lettre en majuscule
    $controller = ucfirst($params[0]).'Controller';

    // On sauvegarde le 2ème paramètre dans $function si il existe, sinon index
    $function = $params[1] ?? 'index';

    if (file_exists(ROOT.'controllers/'.$controller.'.php')) {    // On appelle le contrôleur
        require_once(ROOT.'controllers/'.$controller.'.php');

        // On instancie le contrôleur
        $controller = new $controller();
    } else {
        header('Location:' . WEBROOT);
    }

    if(method_exists($controller, $function)){
        unset($params[0], $params[1]);
        call_user_func_array([$controller, $function], $params);
    } else {
        // sinon on crée ce 3ème paramètre
        $params[2] = $params[1];
        // le 2nd est index
        $params[1] = 'index';
        // donc $function est la fonction index
        $function = $params[1];
        if(method_exists($controller, $function)){
            unset($params[0], $params[1]);
            call_user_func_array([$controller, $function], $params);
        } else {
            // On envoie le code réponse 404
            header('Location:' . WEBROOT);
        }
    }
} else {
    // Ici aucun paramètre n'est défini
    // On appelle le contrôleur par défaut
    require_once(ROOT.'controllers/Main.php');

    // On instancie le contrôleur
    $controller = new Main();

    // On appelle la méthode index
    $controller->index();
}
