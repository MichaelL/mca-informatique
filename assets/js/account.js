const infos = document.getElementById("infos"),
    userInfos = infos.querySelectorAll('input'),
    btnUser = document.getElementById("user");

$(userInfos).change(function () {
    if ($(this).attr('id') !== "old-password") {
        btnUser.removeAttribute('disabled')
    }
})