const navbar = document.getElementById('navbar'),
    footer = document.getElementsByTagName('footer')[0],
    date = new Date().getFullYear();

let listProductId = JSON.parse(sessionStorage.getItem('cart')) || [];

if (navbar) {
    dropdown = navbar.querySelectorAll('.dropdown');
    gradient = ["#36BAE0","#4169e0", "#E06B4C"]
    for (let index = 0; index < dropdown.length; index++) {
        $(dropdown[index].lastElementChild).css({
            background: "linear-gradient(90deg, "+gradient[index]+" 0%, "+dropdown[index].attributes['data-color'].value+" 75%)"
        })

        $(dropdown[index]).hover(function (){
            $(dropdown[index]).css({
                background: "linear-gradient(90deg, "+gradient[index]+" 0%, "+dropdown[index].attributes['data-color'].value+" 75%)"
            })
        }, function () {
            if (!$(dropdown[index]).hasClass('active')) {
                dropdown[index].removeAttribute('style')
            }
        })
    }
}

if (footer) {
    document.getElementById('copyright').textContent = '\u00A9 MCA INFORMATIQUE - ' + date;
}

if (document.getElementsByClassName('cart')[0]) {

    if (sessionStorage.getItem('cart')) {
        let product = $('#addToCart').attr('data-article');

        listProductId.forEach(function (item) {
            if (item === product) {
                $('#addToCart').parent().html('<button class="btn btn-success rounded-0 w-100"><i class="bi bi-cart-check-fill"></i> ARTICLE AJOUTÉ AU PANIER</>')
            }
        })
        $('.cart > span').text(JSON.parse(sessionStorage.getItem('cart')).length)
        $('input[name=cart]').val(sessionStorage.getItem('cart'))
    } else {
        $('.cart > span').text(0)
    }

    $('#addToCart').click(function () {
        let product = $(this).attr('data-article');

        addProduct(product)
        displayNumber()
        $(this).parent().html('<button class="btn btn-success rounded-0 w-100"><i class="bi bi-cart-check-fill"></i> ARTICLE AJOUTÉ AU PANIER</button>')
    })

    $('.removeToCart').click(function () {
        let product = $(this).attr('data-article'),
        price = parseFloat($(this).parents('.bg-white').find('.price')[0].textContent.replace('€', '').trim());
        price = parseFloat($('#total_price').text()) - price;
        $('#total_price').text(price.toFixed(2) + ' €')
        $(this).parents('.bg-white').remove()
        removeProduct(product)
        displayNumber()
        if ($('.removeToCart').length === 0) {
            $('#cartTitle').append('Panier vide');
            $('#total_price').hide();
            $('#commande').hide();
        }
    })

    function addProduct(selectedProduct) {
        $('.cart > span').text(parseInt($('.cart > span').text()) + 1);
        listProductId.push(selectedProduct);
        sessionStorage.setItem('cart', JSON.stringify(listProductId));
        $('input[name=cart]').val(sessionStorage.getItem('cart'));
    }

    function removeProduct(selectedProduct) {
        let index = listProductId.indexOf(selectedProduct);

        listProductId.forEach(function (item) {
            if (item === selectedProduct) {
                $('.cart > span').text(parseInt($('.cart > span').text()) - 1);
                listProductId.splice(index, 1);
                sessionStorage.setItem('cart', JSON.stringify(listProductId));
                $('input[name=cart]').val(sessionStorage.getItem('cart'))
            }
        })
    }

    function displayNumber() {
        if ($('.cart > span').text() == 0) {
            document.getElementsByClassName('cart')[0].children[0].style.display = 'none';
            sessionStorage.removeItem('cart')
        } else {
            document.getElementsByClassName('cart')[0].children[0].style.display = 'block';
        }
    }

    displayNumber()
}

if ($('#total_price')[0]) {
    let prices = document.getElementsByClassName('price'),
        price = 0;
    $(prices).each(function (index, item) {
        price += parseFloat(item.textContent.replace('€', '').trim());
    })
    $('#total_price').text(price + ' €')
}

if ($("#nbChar")[0]) {
    $("#nbChar").text("Plus que " + (255 - $("#article_description").val().length) + " caractères");
    $("#article_description").keyup(function () {
        $("#nbChar").text("Plus que " + (255 - $("#article_description").val().length) + " caractères");
    });
}

if ($('#address')[0]) {
    $('#address').change(function () {
        switch ($('#address').val()) {
            case "al":
                $('#af').addClass('d-none')
                $('#al').removeClass('d-none')
                break
            case "af":
                $('#al').addClass('d-none')
                $('#af').removeClass('d-none')
        }
    })
}

if ($('.view-command')[0]) {
    $('.view-command').click(function () {
        $('#'+$(this).attr('data-command')).toggleClass('d-none')
    })
}