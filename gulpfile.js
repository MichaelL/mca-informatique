const gulp = require('gulp'),
    concat = require('gulp-concat-css'),
    minify = require('gulp-csso');

function css() {
    return gulp.src('assets/css/*.css')
        .pipe(concat('style.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('assets/css/min/'))

}

gulp.task('css', function () {
    return gulp.watch('assets/css/*.css', css);
})